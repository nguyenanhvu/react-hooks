export const getRandomColor = (data) => {
  const randomIndex = Math.trunc(Math.random() * 5);
  return data[randomIndex];
};
