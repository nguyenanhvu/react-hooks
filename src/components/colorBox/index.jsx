import React, { useState } from "react";
import PropTypes from "prop-types";
import { getRandomColor } from "../common/common";
import "./colorBox.scss";

ColorBox.propTypes = {};

const colorList = ["deeppink", "palevioletred", "green", "tomato", "brown"];

function ColorBox(props) {
  const [color, setColor] = useState("deeppink");

  const handleColorBox = () => {
    const newColor = getRandomColor(colorList);
    setColor(newColor);
  };

  return (
    <div
      className="color-box"
      style={{
        backgroundColor: color,
        color: "#fff",
      }}
      onClick={handleColorBox}
    ></div>
  );
}

export default ColorBox;
