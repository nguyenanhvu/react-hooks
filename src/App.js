import "./App.css";
import ColorBox from "./components/colorBox";

function App() {
  return (
    <div className="App">
      <h1>Welcome to React Hooks</h1>
      <ColorBox />
    </div>
  );
}

export default App;
